# Sphinx Phoneme 

This is just a recompilation of original sphinx4 system with a small change to the source code that allows for the
default speech recognizers to provide not only the spoken words, but also the phonemes used by sphinx to generate these words.

## To use

1. Clone the repository
2. Import the project in eclipse using the project from directory or archive option
3. If eclipse shows some errors then you will need to provide it with the needed sphinx jar, this jar is included with the repository (don't use an alternative)
4. Add the mentioned jar above to the project by right clicking the project in the package explorer, selecting build path, add external archives.
5. Run the PhonemeDemo class main method. To recognize different phonemes change the file specified at line 28.

