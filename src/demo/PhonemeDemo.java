package demo;

import java.io.*;
import java.util.*;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;

import javax.swing.plaf.synth.SynthOptionPaneUI;

public class PhonemeDemo {
  public static void main(String args[]) throws IOException {
    Configuration configuration = new Configuration();

    configuration.setAcousticModelPath("en-us/en-us");

    configuration.setDictionaryPath("en-us/cmudict-en-us.dict");
    configuration.setLanguageModelPath("en-us/en-us.lm.bin");

    StreamSpeechRecognizer recognizer = new StreamSpeechRecognizer(configuration);
    InputStream stream;
    Scanner sc = new Scanner(new File("phonedict.txt"));
    Map<String, List<List<String>>> map = new HashMap<>();
    while (sc.hasNext()) {
      String[] lineStr = sc.nextLine().split("\\s");
      String key = lineStr[0];
      List<String> list = new ArrayList<>();
      for (int i = 1; i < lineStr.length; ++i) {
        list.add(lineStr[i]);
      }
      if (key.indexOf("(") == -1) {

        if (!map.containsKey(key)) {

          map.put(key, new ArrayList<>());
        }
        map.get(key).add(list);

      } else {
        key = key.substring(0, key.indexOf("("));
        if (!map.containsKey(key)) {

          map.put(key, new ArrayList<>());
        }
        map.get(key).add(list);
      }
    }

    Map<Integer, List<List<String>>> ground = getGroundTruth(map);

    String[] testwav = new String[25];
    File outFile = new File("res.txt");
    FileWriter fWriter = new FileWriter(outFile);
    PrintWriter pWriter = new PrintWriter(fWriter);
    for (int i = 0; i < 25; ++i) {
      //  System.out.println("index: " + i);
      String tmp = "test_sounds/sentence_test_set/" + (i + 1) + ".wav";
      stream = new FileInputStream(new File(tmp));
      recognizer.startRecognition(stream);
      SpeechResult result;
      String str = "";
      while ((result = recognizer.getResult()) != null) {
        str += result.getPhonemes();
//        System.out.println(result.getPhonemes());
//        System.out.println(result.getHypothesis());
//        System.out.println(result.getHypothesis());
      }
        process(str, map, pWriter, i, ground);
      stream.close();
      recognizer.stopRecognition();
    }
    pWriter.close();
    // Change the file name below to change the sentence used for phoneme recognition
    //    stream = new FileInputStream(new File("test_sounds/2.wav"));
    //    recognizer.startRecognition(stream);
    //  Scanner sc = new Scanner( new File ("dict.txt"));
    //    Set<String> set = new HashSet<>();
    //    while (sc.hasNext()) {
    //
    //      String[] lineStr = sc.nextLine().split("\\s");
    //      for (String s: lineStr) {
    //      	set.add(s);
    //		  System.out.printf(s);
    //	  }
    //    }
    //    sc.close();
    //	  File outFile = new File ("a.txt");
    //	  FileWriter fWriter = new FileWriter (outFile);
    //	  PrintWriter pWriter = new PrintWriter (fWriter);
    //	  for (String s: set) {
    //
    //		  pWriter.println (s);
    //	  }
    //
    //	  pWriter.close();

  }

  static void process(
      String s,
      Map<String, List<List<String>>> map,
      PrintWriter pWriter,
      int rowIndex,
      Map<Integer, List<List<String>>> ground) {
    String[] lineStr = s.split("\\s");
    int number = 0;
    int correctLargest = 0;
    int correctsmallest = 0;
    List<String> tmp = new ArrayList<>();
    for (String t : lineStr) {
      int index1 = t.indexOf("[");
      int index2 = t.indexOf("]");
      String key = t.substring(0, index1).toUpperCase();
      String value = t.substring(index1 + 1, index2);
      String[] v = value.split(",");
      for (String tt: v) {
          tmp.add(tt);
      }
//      int minIndex = Integer.MAX_VALUE;
//      int maxIndex = Integer.MIN_VALUE;
//      if (map.containsKey(key)) {
//        number += v.length;
//        for (List<String> list : map.get(key)) {
//          int right = 0;
//          int len = Math.min(list.size(), v.length);
//          for (int i = 0; i < len; ++i) {
//
//            if (v[i].equals(list.get(i))) {
//              right++;
//            }
//          }
//          minIndex = Math.min(right, minIndex);
//          maxIndex = Math.max(right, maxIndex);
//        }
//        correctLargest += maxIndex;
//        correctsmallest += minIndex;
//      }
    }

      int minIndex = Integer.MAX_VALUE;
      int maxIndex = Integer.MIN_VALUE;
    List<List<String>> tmp2 = ground.get(rowIndex);
    number = tmp.size();
    for (List<String> li : tmp2) {
        int len  = Math.min(tmp.size(),li.size() );
        number = Math.max(number, li.size());
        int right = 0;
        for (int i = 0 ; i < len; ++ i) {
            if (tmp.get(i).equals(li.get(i))) {
                right++;
            }
        }
        minIndex = Math.min(right, minIndex);
        maxIndex = Math.max(right, maxIndex);
    }
    pWriter.println(rowIndex + " " +  (double) maxIndex / number + "  " +  (double) minIndex / number);
//    pWriter.println("hypothesis: " + s);
//      pWriter.println("hypothesis: ");
//    pWriter.println(rowIndex + " :result largest:   " + (double) maxIndex / number);
//    pWriter.println(rowIndex + " : result smallest:   " + (double) minIndex / number);
//    pWriter.println();
//    pWriter.println();
//    pWriter.println();
  }

  static Map<Integer, List<List<String>>> getGroundTruth(Map<String, List<List<String>>> map) {
    Scanner sc = null;
    try {
      sc = new Scanner(new File("dict.txt"));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    Map<Integer, List<List<String>>> ground = new HashMap<>();
    int index = 0;
    while (sc.hasNext()) {
     // System.out.println(index + ": ");
      String[] lineStr = sc.nextLine().split("\\s");
      ground.put(index, new ArrayList<>());
      List<List<String>> res = new ArrayList<>();

      dfs(map, lineStr, 0, res, new ArrayList<>());
      // System.out.println(res);
      ground.get(index).addAll(res);
      index++;
    }
    return ground;
  }

  static void dfs(
      Map<String, List<List<String>>> map,
      String[] arr,
      int pos,
      List<List<String>> res,
      List<String> list) {
    if (pos == arr.length) {
      res.add(list);
      return;
    }
    // System.out.println(arr[pos]);
    for (List<String> lt : map.get(arr[pos].toUpperCase())) {
      List<String> next = new ArrayList<>(list);
      next.addAll(lt);
      dfs(map, arr, pos + 1, res, next);
    }
  }
}
